//
//  MemeCollection.swift
//  Meme Moods
//
//  Created by Sean Wong on 3/3/20.
//  Copyright © 2020 Tinkertanker. All rights reserved.
//

import Foundation
struct MemeCollection: Codable {
    var name: String
    var imageNames: [String]
}
