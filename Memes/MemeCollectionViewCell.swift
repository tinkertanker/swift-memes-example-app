//
//  MemeCollectionViewCell.swift
//  Meme Moods
//
//  Created by Sean Wong on 3/3/20.
//  Copyright © 2020 Tinkertanker. All rights reserved.
//

import UIKit

class MemeCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var memeImageView: UIImageView!
}
